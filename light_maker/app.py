from flask import Flask, render_template, json, request, session, redirect,flash,url_for
from flaskext.mysql import MySQL


mysql = MySQL()
app = Flask(__name__)
app.secret_key = 'why would I tell you my secret key?'

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '4256'
app.config['MYSQL_DATABASE_DB'] = 'BucketList'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/')
def main():
    return render_template('index.html')

@app.route('/refa')
def refa():
    return render_template('refa.html')

@app.route('/resu')
def resu():
    return render_template('resu.html')

@app.route('/showSignUp')
def showSignUp():
    return render_template('signup.html')

@app.route('/showSignin')
def showSignin():
    if session.get('user'):
        return render_template('userHome.html')
    else:
        return render_template('signin.html')

@app.route('/userHome')
def userHome():
    if session.get('user'):
        return render_template('userHome.html')
    else:
        return render_template('error.html',error = 'Unauthorized Access')

@app.route('/main')
def main1():
    if session.get('user'):
        return render_template('main.html')
    else:
        return render_template('error.html',error = 'Unauthorized Access')

@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')


@app.route('/validateLogin', methods=['POST'])
def validateLogin():
    try:
        _username = request.form['inputEmail']
        _password = request.form['inputPassword']

        # connect to mysql

        con = mysql.connect()
        cursor = con.cursor()
        cursor.callproc('sp_validateLogin', (_username,))
        data = cursor.fetchall()

        if  data[0][3]:
            if data[0][3] == _password:
                print(data[0][0])
                session['user'] = True
                print("00")
                return redirect('/userHome')
            else:
                print("11")
                return render_template('error.html', error='Wrong Email address or Password.')
        else:
            print("22")
            return render_template('error.html', error='Wrong Email address or Password.')


    except Exception as e:
        print("33")
        return render_template('error.html', error=str(e))
    finally:
        cursor.close()
        con.close()


@app.route('/signUp', methods=['GET', 'POST'])
def signUp():
    try:
        _name = request.form['inputName']
        _email = request.form['inputEmail']
        _password = request.form['inputPassword']

        # validate the received values
        if _name and _email and _password:
            # All Good, let's call MySQL
            conn = mysql.connect()
            cursor = conn.cursor()

            cursor.callproc('sp_createUser', (_name, _email, _password))
            data = cursor.fetchall()


            if not data:
                conn.commit()
                flash("Success")
                return render_template('resu.html')

            else:
                flash("Fail")
                return render_template('resu.html')
        else:
            print("dd")
            return json.dumps({'html': '<span>Enter the required fields</span>'})
    except Exception as e:

            flash("Fail")
    finally:
        cursor.close()
        conn.close()
if __name__ == "__main__":
    app.run()